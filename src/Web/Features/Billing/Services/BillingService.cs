﻿using Web.Features.Billing.Abstractions;
using Web.Features.Billing.Models;
using System;
using System.Threading;
using System.Threading.Tasks;
using Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Data.Entities;

namespace Web.Features.Billing.Services
{
    public sealed class BillingService : IBillingService
    {
        /// <inheritdoc/>
        public Task<bool> HasAccessAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            var txSum = MyContext.Report.IncomingTxCount + MyContext.Report.OutgoingTxCount;
            BillingTier tier = MyContext.User.GetTierOrDefault(MyContext.Report.TaxYear);

            if (tier?.Threshold >= txSum)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        /// <inheritdoc/>
        public Task<int> CalculatePriceAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
