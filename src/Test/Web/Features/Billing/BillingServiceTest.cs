﻿using Data.Context;
using Data.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using static Test.Extensions.InMemoryContextExtensions;

namespace Test.Web.Features.Billing
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    class BillingServiceTest
    {        

        [Test]
        public void HasAccess_Should_Check_If_True()
        {
            // Arrange
            var txSum_Negative = -5;
            var txSum_Zero = 0;
            var txSum_Greater = 100000;
            var txSum_Smaller = 10;
            var txSum_Equal = 100;

            BillingTier tier_Hobbyist = BillingTier.Create(threshold: 100, name: "Hobbyist", fullAmountInCents: 3999);
            BillingTier tier_null = null;
            // Act
            var result_1 = tier_Hobbyist?.Threshold >= txSum_Negative;
            var result_2 = tier_Hobbyist?.Threshold >= txSum_Zero;
            var result_3 = tier_Hobbyist?.Threshold >= txSum_Greater;
            var result_4 = tier_Hobbyist?.Threshold >= txSum_Smaller;
            var result_5 = tier_Hobbyist?.Threshold >= txSum_Equal;
            
            var result_null = tier_null?.Threshold >= txSum_Zero;


            // Assert
            result_1.Should().BeTrue();
            result_2.Should().BeTrue();
            result_3.Should().BeFalse();
            result_4.Should().BeTrue();
            result_5.Should().BeTrue();
            result_null.Should().BeFalse();

        }
    }
}
